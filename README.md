# AI Aplicada a la generación de Imagen



## Antecedentes Historicos

[El camino del software. Breve historia de la Inteligencia Artificial 3](https://www.linuxadictos.com/el-camino-del-software-breve-historia-de-la-inteligencia-artificial-3.html)

[La primera edad de oro. Breve historia de la Inteligencia Artificial 4](https://www.linuxadictos.com/la-primera-edad-de-oro-breve-historia-de-la-inteligencia-artificial-4.html)



## Redes Neuronales

Las redes neuronales son un tipo de modelo de aprendizaje automático que se inspira en la estructura y funcionamiento del cerebro humano. Estas redes consisten en una serie de nodos, llamados neuronas artificiales, que están conectados entre sí y organizados en capas. Cada neurona recibe una entrada, realiza un cálculo y produce una salida que se transmite a otras neuronas. La salida de una capa de neuronas se convierte en la entrada de la siguiente capa, y así sucesivamente.

A través de un proceso de entrenamiento, las redes neuronales pueden aprender a realizar tareas específicas, como clasificación de imágenes o reconocimiento de voz, ajustando los pesos de las conexiones entre las neuronas. Este proceso se realiza con un conjunto de datos de entrenamiento y se busca minimizar una función de pérdida que mide la diferencia entre las salidas producidas por la red y las salidas esperadas.

Las redes neuronales son muy versátiles y pueden utilizarse en una amplia variedad de aplicaciones de inteligencia artificial, incluyendo reconocimiento de patrones, procesamiento de lenguaje natural, predicción y control de sistemas dinámicos, entre otras.


## Machine Learning 

El machine learning, o aprendizaje automático en español, es una rama de la inteligencia artificial que se centra en el desarrollo de algoritmos que permiten a los sistemas informáticos aprender y mejorar automáticamente a partir de datos.

En lugar de programar explícitamente una serie de reglas y algoritmos para realizar una tarea específica, en el aprendizaje automático se proporciona un conjunto de datos de entrenamiento y se busca que el sistema aprenda automáticamente a partir de ellos, identificando patrones y relaciones en los datos.

Existen diferentes tipos de aprendizaje automático, incluyendo el aprendizaje supervisado, el aprendizaje no supervisado y el aprendizaje por refuerzo. En el aprendizaje supervisado, se proporcionan ejemplos etiquetados al sistema, es decir, se le dice cuál es la salida correcta para cada entrada, y se busca que el sistema aprenda a producir la salida correcta para nuevas entradas. En el aprendizaje no supervisado, no se proporcionan etiquetas, y el sistema debe encontrar patrones y estructuras en los datos por sí solo. En el aprendizaje por refuerzo, el sistema recibe una recompensa o castigo por sus acciones y aprende a optimizar su comportamiento para maximizar la recompensa.


### REQUERIMIENTOS PARA EL TALLER

*  Una  cuenta de google con mas de 5 gb deespacio en el drive]

* Una cuenta de [github](https://github.com/)

*  (Recomendado) Una cuenta de [Discord](https://discord.com/)

* (Recomenado) [ [Una cuenta de HuggingFace](https://huggingface.co/) ]

## Herramientas de base 

[pytorch](https://pytorch.org/)

[Keras](https://keras.io/)

[scikit learn](https://scikit-learn.org/stable/)

[Tensorflow](https://www.tensorflow.org/)

[Darknet](https://pjreddie.com/darknet/)

[Open Cv](https://opencv.org/)

[VIdeo](https://www.youtube.com/watch?v=SVcsDDABEkM)


# AI Y cambio climatico 

[Referente 1](https://www.biodiversidadla.org/Recomendamos/Inteligencia-artificial-aumenta-el-caos-climatico)



# Software privativo y Software Open source


## Dalee

[Dalee](hhttps://openai.com/product/dall-e-2)

## Midjourney

[MJ](https://www.midjourney.com/)

## StableDiffusion


[SD](https://stability.ai/)



## Prompt


## Moldes o Modelos


## Tools Online 

[Dreamstudio](https://beta.dreamstudio.ai/generate)

[Playground](https://playgroundai.com/create)

[Leonardo](https://leonardo.ai/)

[FULLPACK TOOLS](https://pharmapsychotic.com/tools.html)




